/*
Max-heap implemntation using array
max size of 32
*/

public class MaxHeap {
	//array to hold the heap
	private int[] heap;
	//current size
	private int size;
     
	//Initialize the heap
	public MaxHeap(){
        	size = 0;
        	heap = new int[32];
	}
    
	//return index of the parent of node i 
	private int parent(int i){
        	return (i-1)/2;
    	}
     	
	//return index of the left child of node i
    	private int left(int i){
        	return 2*i + 1;
	}

        //return index of the right child of node i
        private int right(int i){
                return 2*i + 2;
        }

	//swap 2 nodes
	private void swap(int i, int j){
		int tmp = heap[i];
		heap[i] = heap[j];
		heap[j] = tmp;
	}
     
	//insert a node 
	public void insert(int key){
        	heap[size] = key;
		size++;
	
		//buble up if needed
		int current = size - 1;
		int p = parent(current);

		while(true){
			//reach the root
			if (p<0){
				break;
			}
			//swap parent and child value
			if(heap[p] < heap[current]){
				swap(current,p);
				//set up next iteration			
				current = p;
				p = parent(current);
			}
			else{
				//no need to bubble up further
				break;
			}
		}
 	}
     
	//remove the root from the heap
	public int remove(){
		//swap the root with the last node
		swap(0,size-1);
		//decrease the size of the heap
		size = size - 1;	
		
		//buble down from the top  if needed
		int current = 0;
		int l = left(current);
		int r = right(current);
		
		while(true){
			//reach the leaf node
			if(l>size-1){
				break;
			}

			//parent only has a left node
			if(r>size-1){
				//parent node >= left, stop
				if(heap[current]>=heap[l]){
					break;
				}
				//swap and done
				else{
					swap(current,l);
					break;
				}
			}	
			
			//parent has both left and right node
			//parent node > both left and right node
			//no need to bubble down further
			if(heap[current]>=heap[l] && heap[current]>=heap[r]){
				break;
			}
			//else keep bubble down the left side
			else if(heap[current] < heap[l] && heap[l]>heap[r]){
				swap(current,l);
				//set up next iteration
				current = l;
				l = left(current);
				r = right(current);
			}
			//bubble down the right side
                        else if(heap[current] < heap[r]){
                                swap(current,r);
                                //set up next iteration
                                current = r;
                                l = left(current);
                                r = right(current);
			}
		}
		return heap[size];
	}
     	

	//print the heap
	public void printHeap()
        {	
		for (int i = 0; i < size; i++){
			System.out.print(heap[i] +" ");
		}           
		System.out.println();
	}
      
	public static void main(String[] args){
		MaxHeap maxHeap = new MaxHeap();

		maxHeap.insert(4);
		maxHeap.insert(19);
		maxHeap.insert(20);
		maxHeap.insert(22);
		maxHeap.insert(7);
		maxHeap.insert(11);
		maxHeap.insert(40);
		maxHeap.insert(39);
		maxHeap.insert(8);
		maxHeap.insert(15);
	
		int s = 10;

		//remove() the root of the heap 10 times, and print out
		for(int i = 0 ; i < 10; i++){
			//maxHeap.printHeap();
			System.out.println(maxHeap.remove());
         	} 
     	}
}
